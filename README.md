﻿# CSCJackpot

The lottery system integrates blockchain techonologies.

## Getting Started

Em note vai huong dan ve viec run project tu visual studio

### Prerequisites

Yêu cầu hệ thống, phần mềm cần phải cài đặt, code mẫu

```
Give examples
```

### Installing

Hướng dẫn run store, script để setup hệ thống, các bước cần thiết để chạy hệ thống

```
Give the example
```

And repeat

```
until finished
```

## How to tag a version
For more details, please refer to https://semver.org/spec/v2.0.0.html

1. MAJOR version when you make incompatible document changes.
2. MINOR version when you add functionality/changes logic.
3. PATCH version when you make backwards-compatible bug fixes.

**[MAJOR.MINOR.PATCH]** format.

## Running the tests

None

### Break down into end to end tests

None

## Commit code process
For more details, please refer to https://github.com/trein/dev-best-practices/wiki/Git-Commit-Best-Practices

1.  Ensure pull code from Repository.
2.  Ensure not any issues from your code.
3.  Only commit when your code reaches a codebase (https://en.wikipedia.org/wiki/Codebase) means dont commit half done work.
4.  Commit with the meaning comments. Follow the example

```

What changes:
- Filenames 
- Class
- Method/function

Why those changes nessecsary:
- Create New / Fix bug at abc() from AbcClass / Add logic/ Remove ... so that follows client requirements, 

What side effects:
- Xyz()
- ConfigClass
- Database


```

> Remember this best practice ***"Branch out, merge often and keep always in sync"***

## Deployment QA environment

1.
2.
3.
4.
5.

## Deployment Live environment

1. 
2.
3.
4.
5.

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details



## CSCJackPot config
+ Những thông tin chính cần config khi up lên server CSC JACKPOT:
I.	Web FrontEnd:
1.	Danh sách các key trong AppSetting.config. Lưu ý khi thêm mới key nào thì thêm vào key đó. Chú ý đổi lại value của các key sau khi clone ra môi trường mới:
-	Redis:Host
-	wcf:WSSentmailServices
-	Host:Webservices
-	Đường dẫn log, publish key, private key của RSA, Logs File
-	Các key bên Ví
2.	Khi build những function mới không được up lại toàn bộ, chỉ up những function nào thêm mới hoặc chỉnh sữa.

II.	Services FrontEnd:
-	Web config lưu ý đường dẫn private key của RSA
III.	Web Admin:
1.	Danh sách các key trong Web.config. Lưu ý khi thêm mới key nào thì thêm vào key đó. Chú ý đổi lại value của các key sau khi clone ra môi trường mới:
-	Redis:Host
-	wcf:WSSentmailServices
-	Host:Webservices
-	Đường dẫn log, publish key, private key của RSA
-	Các key bên Ví
2.	Khi build những function mới không được up lại toàn bộ, chỉ up những function nào thêm mới hoặc chỉnh sữa.
-	
IV.	Services Admin:
-	Web config lưu ý đường dẫn private key, publish key của RSA, Logs file

***+++ Lưu ý: Nhớ add IP (2 services) vào bảng IPConfigs


+ Những thông tin cần thiết khi đưa code win services lấy kết quả xổ số tự động:
Tạo dữ liệu khởi tạo:
-	Đưa tất cả các ví trong hệ thống cwallet về giá trị khởi tạo: 
o	sysGuaranteeFund = 0
o	sysProgressivePrize = 0
o	sysFeeTxn = 0
o	sysMgmt = 0
o	sysAdmin= 0
o	sysAdward=0
o	sysFeePrize=0
o	sysJackPot = 10 (default)
o	sysFreeTicket =  1 (default)

-	Xóa tất cả các transaction trong hệ thống cWallet
-	Khởi tạo dữ liệu trên cscjackpot:
o	Xóa dữ liệu bảng :
	AwardMegalBall
	AwardMegaballLog
	BookingMegaball
	Draw
	FreeTicketDaily
	TicketNumberTimeLog
	AwardWithdraw	
o	Khởi tạo dữ liệu:
	SystemConfig (bật qua chế độ IsAutoLottery = 1)
	Draw
	FreeTicketDaily
	StoreFreeTicket
-	Ghi chú khi gắn key config:
o	Đường dẫn pathlog, PATH_LOG
o	Đường dẫn file public.xml
o	Open date: Tuesday,Thursday ,Saturday
o	HoureStart: 13
o	MinuteStart: 0
o	HourEnd: 13
o	MinuteEnd: 30
o	TimeClosedBuyTicket: 12
o	TimeOpenBuyTicket: 14
o	NumberTicketAddDaily: 714
o	Host:Webservices trỏ tới web services của admin (cms), kiểm tra đăng ký trong bảng ipconfig 
o	End point services trỏ tới services admin, và services cWallet
o	NewtonJson (10.0)
o	Redis host
o	    NumberBallNormal = 39
o	    NumberBallGold  = 18
