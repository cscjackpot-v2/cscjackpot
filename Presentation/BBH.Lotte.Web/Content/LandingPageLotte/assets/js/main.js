$(document).ready(function () {
    var widthWindow = $(window).width();
    var heightWindow = $(window).height();
    $('.header').css({ 'min-height': heightWindow + 'px' });

    $('.nav-button').click(function () {
        $(this).find('#nav-icon').toggleClass('open');

        $('body').find('#menu-right').toggleClass('open');
        $('body').toggleClass('open');
    });




    //$('#slick-winner-story').slick({
    //    slidesToShow: 3,
    //    slidesToScroll: 1,
    //    autoplay: true,
    //    autoplaySpeed: 3000,

    //    responsive: [
    //        {
    //            breakpoint: 900,
    //            settings: {
    //                slidesToShow: 2
    //            }
    //        },
    //        {
    //            breakpoint: 650,
    //            settings: {
    //                slidesToShow: 1
    //            }
    //        }]
    //});

    $('#slick-term-story').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 3000,

        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 650,
                settings: {
                    slidesToShow: 1
                }
            }]
    });

    

    $('.slick-partner').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 3000,

        responsive: [
            {
                breakpoint: 900,
                settings: {
                    slidesToShow: 1
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1
                }
            },
            {
                breakpoint: 500,
                settings: {
                    slidesToShow: 1
                }
            }]
    });


    $('[data-toggle="tooltip"]').tooltip()

});


//(function ($) {
//    var date = new Date(),
//        month = date.getMonth();
//    day = date.getDate(),
//        weekDay = date.getDay(),
//        hours = {
//            start: new Date(date.getFullYear(), month, day),
//            end: new Date(date.getFullYear(), month, day)
//        };

//    // weekDay var [0 = sun, 1 = mon, 2 = tues ... 5 = fri 6 = sat]

//    // If it's Monday - Friday
//    if (weekDay >= 1 && weekDay <= 5) {

//        // Start at 7am, end at 8pm
//        hours.start.setHours(7);
//        hours.end.setHours(20);

//        // If it's Saturday
//    } else if (weekDay == 6) {

//        // Start at 8am, end at 8pm
//        hours.start.setHours(8);
//        hours.end.setHours(20);

//        // If it's Sunday
//    } else {

//        // Start at 9am, end at 6pm
//        hours.start.setHours(9);
//        hours.end.setHours(18);
//    }
//})(jQuery);