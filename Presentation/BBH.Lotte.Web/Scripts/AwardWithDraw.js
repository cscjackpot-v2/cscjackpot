﻿
function ResetFrom(id) {
    if (id == 1) {
        // $('#txtAddressWallet').css('border-color', 'antiquewhite');
        $('#lbErrorWallet').text('');
        $('#lbErrorWallet').css('display', 'none');
    }
}
//function ResetForm(id, value) {
//    $('#lbErrorWallet' + id).text('');
//    $('#lbErrorWallet' + id).css('display', 'none');
//}
var maxItemNotification = 1;
function InsertAwardWithDraw() {
    var check = true;
    var bookingID = $('#hdBookingID').val();
    var oppenDateAward = $('#hdOpenDateAward').val();
    var amount = $('#txtAmount').text();
    var awardFee = $('#txtFee').text();
    var addressWallet = $('#txtAddressWallet').text();
    var priority = $('#txtpriority').text();
    var intDrawID = $('#txtDraw').text();
    $.ajax({
        type: "post",
        async: true,
        url: "/Member/InsertAward",
        data: { amount: amount, awardFee: awardFee, addressWallet: addressWallet, oppenDateAward: oppenDateAward, bookingID: bookingID, priority: priority, intDrawID: intDrawID },
        beforeSend: function () {
            showLoadingNew();
        },
        success: function (data) {
            hideLoadingNew();
            if (data.objResult == "InsertSuccess") {
                alertify.success("Insert success");
                setTimeout(function () { window.location.reload(); }, 2000);
            }
            else if (data.objResult == 'InsertFailse') {
                alertify.error('Insert failse');
            }
            else if (data.objResult == 'Failse') {
                alertify.error('Amount not enoungh available');
            }
            else if (data.objResult == 'AmountFails') {
                alertify.error('Value of award is not match. Please check again!');
            }
        },
        error: function (e) {

        }
    });
}

function ShowBookingWinnerDetail(bookingID, valuePoints, awardFee, firstNumber, secondNumber, thirdNumber, fourthNumber, fivethNumber, extraNumber, openDate, btcAddress, priority, intDrawID) {
    $('#hdBookingID').val(bookingID);
    $('#hdOpenDateAward').val(openDate);
    if (typeof $('#standardModal') != 'undefined' && $('#standardModal').length > 0) {
        $('#standardModal').show();
    }
    // $('#standardModal').css("display", "block");
    $('#hdBookingWinnigID').val(bookingID);

    var strTicket = '<span class="ticketNumber">' + firstNumber + '</span>&nbsp;<span class="ticketNumber">' + secondNumber + '</span>&nbsp;<span class="ticketNumber">' + thirdNumber + '</span>&nbsp;<span class="ticketNumber">' + fourthNumber + '</span>&nbsp;<span class="ticketNumber">' + fivethNumber + '</span>&nbsp;<span class="ticketNumber oneMore">' + extraNumber + '</span>';
    $('#txtAmount').text(valuePoints);
    $('#divBallsWinning').html(strTicket);
    $('#txtFee').text(awardFee);
    $('#txtAddressWallet').text(btcAddress);
    $('#txtpriority').text(priority);
    $('#txtDraw').text(intDrawID);
    //$('#txtStatus').val(statusName);
}