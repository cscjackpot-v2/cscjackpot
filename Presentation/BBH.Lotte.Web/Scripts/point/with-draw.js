﻿var maxItemNotification = 1;
$(document).ready(function () {
    $(function () {
        var $formBTC = $('#formWithdrawBTC');
        if ($.validator.unobtrusive != undefined) {
            $.validator.unobtrusive.parse($formBTC);
        }
        $formBTC.unbind('submit').submit(function () {
            showLoadingNew();
            if ($formBTC.valid()) {
                $.ajax({
                    url: "/Point/PartialWithDrawBTCSubmit",
                    async: true,
                    type: 'POST',
                    data: $(this).serialize(),
                    beforeSend: function (xhr, opts) {
                        
                    },
                    complete: function () { },
                    success: function (data) {
                        if (data.isLogin) {
                            if (data.success) {
                                alertify.success('Withdrawal successfully.');
                                checkMaxShowAlertNoti(maxItemNotification);
                                hideLoadingNew();
                                setTimeout(function () {
                                    window.location.reload();
                                }, 1000);
                            }
                            else {
                                alertify.error('Withdrawal fails! Please try again.');
                                checkMaxShowAlertNoti(maxItemNotification);
                                hideLoadingNew();
                            }
                        }
                        else {
                            alertify.error('Plase login to continue.');
                            checkMaxShowAlertNoti(maxItemNotification);
                            hideLoadingNew();
                        }
                    },
                    error: function (data) {
                        hideLoadingNew();
                        alertify.error('An error occurred! Please try again after.');
                        checkMaxShowAlertNoti(maxItemNotification);
                    }
                });
            }
            hideLoadingNew();
            return false;
        });
    });
    $(function () {
        var $formCarcoin = $('#formWithdrawCarCoin');
        if ($.validator.unobtrusive != undefined) {
            $.validator.unobtrusive.parse($formCarcoin);
        }
        $formCarcoin.unbind('submit').submit(function () {
            if ($formCarcoin.valid()) {
                $.ajax({
                    url: "/Point/PartialWithDrawCarcoinSubmit",
                    async: true,
                    type: 'POST',
                    data: $(this).serialize(),
                    beforeSend: function (xhr, opts) {
                        showLoadingNew();
                    },
                    complete: function () { },
                    success: function (data) {

                        if (data.isLogin) {
                            if (data.success) {
                                alertify.success('Withdrawal successfully.');
                                checkMaxShowAlertNoti(maxItemNotification);
                                hideLoadingNew();
                                setTimeout(function () {
                                    window.location.reload();
                                }, 1000);
                            }
                            else {
                                alertify.error('Withdrawal fails! Please try again.');
                                checkMaxShowAlertNoti(maxItemNotification);
                                hideLoadingNew();
                            }
                        }
                        else {
                            alertify.error('Plase login to continue.');
                            checkMaxShowAlertNoti(maxItemNotification);
                            hideLoadingNew();
                        }
                    },
                    error: function (data) {
                        hideLoadingNew();
                        alertify.error('An error occurred! Please try again after.');
                        checkMaxShowAlertNoti(maxItemNotification);
                    }
                });
            }
            return false;
        });
    });
    $(function () {
        $(document).on("cut copy paste", "#AmountBTC", function (e) {
            e.preventDefault();
        });
    });
    $(function () {
        $(document).on("cut copy paste", "#AmountCarCoin", function (e) {
            e.preventDefault();
        });
    });
})
function OnChangeAmountBTC(e) {
    var AmountBTC = $(e).val();
    var Check = AmountBTC / 3;
    var count = 0;
    var numberDecimal = AmountBTC.toString().split(".")[1];
    if (undefined !== numberDecimal) {
        count = numberDecimal.length;
    }
    if (count > 8) {
        $(e).val(AmountBTC.slice(0, -1));
    }
    if (Check.toString() == "NaN") {
        $(e).val(AmountBTC.slice(0, -1));
    }
    else {
        var FeeBTC = $("#hdFeeBTC").val();
        $("#TotalBTC").val((AmountBTC - FeeBTC).toFixed(8));
    }

}
function OnChangeAmountCarCoin(e) {
    var AmountCarcoin = $(e).val();
    var Check = AmountCarcoin / 3;
    var count = 0;
    var numberDecimal = AmountCarcoin.toString().split(".")[1];

    if (undefined !== numberDecimal) {
        count = numberDecimal.length;
    }
    if (count > 8) {
        $(e).val(AmountCarcoin.slice(0, -1));
    }
    if (Check.toString() == "NaN") {
        $(e).val(AmountCarcoin.slice(0, -1));
    }
    else {
        var FeeCarcoin = $("#hdFeeCarcoin").val();
        $("#TotalCarCoin").val((AmountCarcoin - FeeCarcoin).toFixed(8));
    }
}
