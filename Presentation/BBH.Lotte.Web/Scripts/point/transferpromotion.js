﻿var maxItemNotification = 1;

$(document).ready(function () {
    $(function () {
        var $formBTC = $('#formTransferPromotion');
        if ($.validator.unobtrusive != undefined) {
            $.validator.unobtrusive.parse($formBTC);
        }
        $formBTC.unbind('submit').submit(function () {
            showLoadingNew();
            if ($formBTC.valid()) {
                $.ajax({
                    url: "/Point/PartialTransferPromotionSubmit",
                    async: false,
                    type: 'POST',
                    data: $(this).serialize(),
                    beforeSend: function (xhr, opts) {

                    },
                    complete: function () { },
                    success: function (data) {
                        if (data.isLogin) {
                            if (data.success) {
                                alertify.success('Transfer successfully.');
                                checkMaxShowAlertNoti(maxItemNotification);
                                hideLoadingNew();
                                setTimeout(function () {
                                    window.location.reload();
                                }, 1000);
                            }
                            else {
                                if (data.Errorstatus == 1) {
                                    alertify.error('You can not transfer money to yourself');
                                    checkMaxShowAlertNoti(maxItemNotification);
                                    hideLoadingNew();
                                }
                                else if (data.Errorstatus == 2) {
                                    alertify.error('Daily transfer btc limit: 5 BTC/day');
                                    checkMaxShowAlertNoti(maxItemNotification);
                                    hideLoadingNew();
                                }
                                else if (data.Errorstatus == 3) {
                                    alertify.error('Email not exists in system.');
                                    checkMaxShowAlertNoti(maxItemNotification);
                                    hideLoadingNew();
                                }
                                else if (data.Errorstatus == 0) {
                                    alertify.error('Transfer fails! Please try again.');
                                    checkMaxShowAlertNoti(maxItemNotification);
                                    hideLoadingNew();
                                }
                            }
                        }
                        else {
                            alertify.error('Plase login to continue.');
                            checkMaxShowAlertNoti(maxItemNotification);
                            hideLoadingNew();
                        }
                    },
                    error: function (data) {
                        hideLoadingNew();
                        alertify.error('An error occurred! Please try again after.');
                        checkMaxShowAlertNoti(maxItemNotification);
                    }
                });
            }
            hideLoadingNew();
            return false;
        });
    });
    $(function () {
        $(document).on("cut copy paste", "#AmountBTC", function (e) {
            e.preventDefault();
        });
    });
    $(function () {
        $("#spanAvailableTranfer").text($("#hdNumberCoinPromotion").val());
    })
})
function OnChangeAmountBTC(e) {
    var AmountBTC = $(e).val();
    var Check = AmountBTC / 3;
    var count = 0;
    var numberDecimal = AmountBTC.toString().split(".")[1];
    if (undefined !== numberDecimal) {
        count = numberDecimal.length;
    }
    if (count > 8) {
        $(e).val(AmountBTC.slice(0, -1));
    }
    if (Check.toString() == "NaN") {
        $(e).val(AmountBTC.slice(0, -1));
    }
    else {
        var FeeBTC = $("#hdFeeBTC").val();
        $("#TotalBTC").val((AmountBTC - FeeBTC).toFixed(8));
    }

}

function OnClickIsEmail(element) {
    $("#spanTouser").text('');
    if (element.id == "IsEmailTransfer_true") {
        $("#spanUser").text('email');
        $("#IsEmailTransfer_true").attr('checked', true);
        $("#IsEmailTransfer_false").attr('checked', false);
    }
    //if (element.id == "IsEmailTransfer_false") {
    //    $("#spanUser").text('address');
    //    $("#IsEmailTransfer_true").attr('checked', false);
    //    $("#IsEmailTransfer_false").attr('checked', true);
    //}
}