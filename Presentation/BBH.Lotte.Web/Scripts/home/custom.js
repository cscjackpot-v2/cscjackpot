jQuery(document).ready(function () {
    // playVideo();
    // getimg();
    // subscribeForm();
    // getLocation();
    showMenu();
    loadmore();
    toTopButton();
});


/******************************
 ***** Show error if have *****
 ******************************/


window.onerror = function (msg, url, linenumber) {
    if (typeof (console) != 'undefined') {
        console.log('Error message: ' + msg + '\nURL: ' + url + '\nLine Number: ' + linenumber);
    }
    return false;
};

/******************************
 ***** Functions built in *****
 ******************************/

function showMenu() {
    $('.show-menu').click(function () {
        var hidden = $('.main-menu-show').data('hidden');
        // $('.show-menu').text(hidden ? 'Hide Menu' : 'Show Menu');
        if (hidden) {
            $('.main-menu-show').animate({
                left: '-300px'
            }, 500)
        } else {
            $('.main-menu-show').animate({
                left: '0px'
            }, 500)
        }
        $('.main-menu-show,.image').data("hidden", !hidden);

    });
    $('.main-menu-show a').click(function () {
        var hidden = $('.main-menu-show').data('hidden');
        // $('.show-menu').text(hidden ? 'Hide Menu' : 'Show Menu');
        if (hidden) {
            $('.main-menu-show').animate({
                left: '-300px'
            }, 500)
        } else {
            $('.main-menu-show').animate({
                left: '0px'
            }, 500)
        }
        $('.main-menu-show,.image').data("hidden", !hidden);
        // if ($(this).parent().hasClass('contact-link')) {
        //     if ($(window).width() < 980) {
        //         $([document.documentElement, document.body]).animate({
        //             scrollTop: $('#contactus').offset().top - 100
        //         }, 1500);
        //     } else {
        //         $([document.documentElement, document.body]).animate({
        //             scrollTop: $('#contactus').offset().top - 150
        //         }, 1500);
        //     }
        // } else {
        //     if ($(window).width() < 980) {
        //         $([document.documentElement, document.body]).animate({
        //             scrollTop: $($(this).attr("href")).offset().top - 100
        //         }, 1500);
        //     }
        // }
        // if ($(window).width() < 980) {
        //     $([document.documentElement, document.body]).animate({
        //         scrollTop: $($(this).attr("href")).offset().top - 80
        //     }, 1500);
        // }
        // if ($(window).width() < 450) {
        //     $([document.documentElement, document.body]).animate({
        //         scrollTop: $($(this).attr("href")).offset().top - 50
        //     }, 1500);
        // }
    });
    $('#main-content').click(function () {
        var hidden = $('.main-menu-show').data('hidden');
        if (hidden) {
            console.log(12);
            $('.main-menu-show').animate({
                left: '-300px'
            }, 500)
            $('.main-menu-show,.image').data("hidden", !hidden);
        }
    });
}

function loadmore() {
    $('.load-more').click(function () {
        var page = $(this).data('page');
        jQuery.ajax({
            type: 'POST',
            url: '/wp-admin/admin-ajax.php',
            data: {action: 'loadMore', 'page': page},
            beforeSend: function () {
                jQuery('.load-more').addClass('d-none');
            },
            success: function (response) {
                // console.log(response);
                // return false;
                if (response) {
                    var data = jQuery.parseJSON(response);
                    if (data.err == 0) {
                        $('.load-more').data('page', page + 1)
                        $('#list_case_studies').append(data.html)
                        if (data.show == 1) {
                            jQuery('.load-more').removeClass('d-none');
                        }
                    }
                }
            },
            error: function () {
                alert("an error occurred, please try again");
                location.reload();
            }
        });

    });
}

function toTopButton() {
    var scrollTop = $(".scrollTop");

    $(window).scroll(function () {
        // declare variable
        var topPos = $(this).scrollTop();
        // if user scrolls down - show scroll to top button
        if (topPos > 100) {
            $(scrollTop).css("opacity", "1");

        } else {
            $(scrollTop).css("opacity", "0");
        }

    }); // scroll END

    //Click event to scroll to top
    $(scrollTop).click(function () {
        $('html, body').animate({
            scrollTop: 0
        }, 800);
        return false;
    }); // click() scroll top EMD
}