﻿$(document).ready(function () {
    GetExchangeBtcToUsd();

    var current = location.pathname;
    $('#menu-right li a').each(function () {
        var $this = $(this);
        var href = $this.attr('href');
        if (href == current) {
            $this.parent().addClass('active');
        }
    });
});

function GetExchangeBtcToUsd() {
    $.ajax({
        type: "post",
        url: "/Home/CurrencyToBtc",
        async: false,
        contentType: 'application/json',
        beforeSend: function () {
        },
        success: function (data) {
            if (typeof data != 'undefined') {
                var jackpot = $("#hdJackpot").val();
                if (jackpot != 'undefined') {
                    var usd = parseFloat(jackpot) / parseFloat(data); 
                    $("#spanUSD").text('$' + parseFloat(usd, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
                    $("#spanUSDLeft").text('$' + parseFloat(usd, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
                }
            }
        },
        error: function () {
        }
    });
}