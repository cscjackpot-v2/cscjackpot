﻿var maxItemNotification = 1;
$(document).ready(function () {
    $(function () {
        var $formLoginMember = $('#frLoginMember');

        $formLoginMember.unbind('submit').submit(function () {
            if ($formLoginMember.valid()) {
                $.ajax({
                    url: "/Member/SubmitLoginMember",
                    async: true,
                    type: 'POST',
                    data: $(this).serialize(),
                    beforeSend: function (xhr, opts) {
                        showLoadingNew();
                    },
                    complete: function () {
                    },
                    success: function (data) {
                        if (data.login) {
                            if (data.success) {
                                alertify.success('Login successfully.');
                                hideLoadingNew();
                                //setTimeout(function () {
                                window.location.href = '/play-game';
                                //}, 2000);
                            }
                            else {
                                alertify.error('Login fails! Please try again.');
                                hideLoadingNew();
                            }
                        }
                        else {
                            alertify.error('Username and password is incorrect.');
                            hideLoadingNew();
                        }
                    },
                    error: function (data) {
                        alertify.error('An error occurred! Please try again after.');
                        checkMaxShowAlertNoti(maxItemNotification);
                        hideLoadingNew();
                    }
                });
            }
            return false;
        });
    });
});