﻿$(document).ready(function () {
    $(function () {
        var $formRegister = $('#frRegister');
        $('#registration-login').change(
            function () {
                $formRegister.removeData('validator');
                $formRegister.removeData('unobtrusiveValidation');
                $.validator.unobtrusive.parse($formRegister);
            });
        $formRegister.unbind('submit').submit(function () {
            if ($formRegister.valid()) {
                $.ajax({
                    url: "/Member/MemberRegister",
                    async: true,
                    type: 'POST',
                    data: $(this).serialize(),
                    beforeSend: function (xhr, opts) {
                        showLoadingNew();
                    },
                    complete: function () { },
                    success: function (data) {
                        if (!data.ExistEmail) {
                            if (!data.ExistUserName) {
                                if (data.success) {
                                    alertify.success('You have successfully registered. Please check your email.');
                                    checkMaxShowAlertNoti(maxItemNotification);
                                    hideLoadingNew();
                                    setTimeout(function () {
                                        window.location.href = '/logout';
                                    }, 2000);
                                }
                                else {
                                    alertify.error('Register fails! Please try again.');
                                    checkMaxShowAlertNoti(maxItemNotification);
                                    hideLoadingNew();
                                }
                            }
                            else {
                                alertify.error('Your username is already in use.');
                                checkMaxShowAlertNoti(maxItemNotification);
                                hideLoadingNew();
                            }
                        }
                        else {
                            alertify.error('Your email address is already in use.');
                            checkMaxShowAlertNoti(maxItemNotification);
                            hideLoadingNew();
                        }
                    },
                    error: function (data) {
                        hideLoadingNew();
                        alertify.error('An error occurred! Please try again after.');
                        checkMaxShowAlertNoti(maxItemNotification);
                    }
                });
            }
            return false;
        });
    });
});