﻿$(document).ready(function () {
    $(function () {
        var $formUpdatePassword = $('#frUpdatePassword');

        $formUpdatePassword.unbind('submit').submit(function () {
            if ($formUpdatePassword.valid()) {
                $.ajax({
                    url: "/Member/UpdatePassword",
                    async: true,
                    type: 'POST',
                    data: $(this).serialize(),
                    beforeSend: function (xhr, opts) {
                        setTimeout(function () {
                            checkMaxShowAlertNoti(maxItemNotification);
                            showLoadingNew();
                        }, 2000);
                    },
                    complete: function () {
                    },
                    success: function (data) {
                        if (data.success) {
                            alertify.success('Update password successfully.');
                            checkMaxShowAlertNoti(maxItemNotification);
                            hideLoadingNew();
                            window.location.reload();
                            //setTimeout(function () {
                            //    window.location.href = '/logout';
                            //}, 2000);
                        }
                        else {
                            alertify.error('Update fails! Please try again.');
                            checkMaxShowAlertNoti(maxItemNotification);
                            hideLoadingNew();
                        }
                    },
                    error: function (data) {
                        alertify.error('An error occurred! Please try again after.');
                        hideLoadingNew();
                    }
                });
            }
            return false;
        });
    });
});