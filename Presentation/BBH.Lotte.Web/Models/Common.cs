﻿using BBC.Core.Common.Log;
using BBC.Core.Database;
using BBC.CWallet.ServiceListener.ServiceContract;
using BBH.Lotte.CLP.Wallet.Repository;
using BBH.Lotte.Shared;
using Newtonsoft.Json;
using System;
using System.Configuration;

namespace BBH.Lotte.Web.Models
{
    public class Common
    {
        public static string ClientID = ConfigurationManager.AppSettings[KeyManager.ClientID];
        public static string SystemID = ConfigurationManager.AppSettings[KeyManager.SystemID];
        public static string WalletID = ConfigurationManager.AppSettings[KeyManager.WalletID];
        public static string RequestBy = ConfigurationManager.AppSettings[KeyManager.RequestBy];

        public const string KeyListAward = "KeyListAward";
        public const string KeyListBooking = "KeyListBooking";
        public const string KeyListTransactionCoin = "KeyListTransactionCoin";
        public const string KeyListAwardBO = "KeyListAwardBO";
        public const string KeyTicketConfig = "KeyTicketConfig";

        public const string KeyAwardWithdraw = "KeyAwardWithdraw";

        public const string KeyAwardWithdrawDetail = "KeyAwardWithdrawDetail_";
        public const string ListCacheBookingMember = "ListCacheBookingMember";

        public const string KeyBooking = "KeyBooking";
        public const string KeyAward = "KeyAward";

        public static T ParseObjectFromAsync<T>(ObjResultMessage objResult)
        {

            if (objResult == null || objResult.ObjResultData == null)
                return default(T);

            string strJson = JsonConvert.SerializeObject(objResult.ObjResultData);

            return JsonConvert.DeserializeObject<T>(strJson);
        }

        public static string GetLocalIPAddress()
        {
            string ipAddress = "";
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }

            return context.Request.ServerVariables["REMOTE_ADDR"];
        }

        public static string GetHostServices()
        {
            string hostServices = string.Empty;
            string servicesName = string.Empty;

            hostServices = ConfigurationManager.AppSettings["Host:Webservices"];
            servicesName = ConfigurationManager.AppSettings["Host:ServicesName"];
            hostServices = hostServices + "/" + servicesName;
            return hostServices;
        }

        public static AddressInfo GetAddressInfo(string strUserID)
        {
            AddressInfo objAddressInfo = new AddressInfo();
            try
            {
                AddressInfoRequest objAddressInfoRequest = new AddressInfoRequest();
                objAddressInfoRequest.CurrencyCode = CurrencyCode.BTC;
                objAddressInfoRequest.UserID = strUserID;
                objAddressInfoRequest.ClientID = short.Parse(ClientID);
                objAddressInfoRequest.SystemID = short.Parse(SystemID);
                objAddressInfoRequest.IsCreateAddr = false;// no check create new account
                objAddressInfoRequest.CreatedBy = strUserID;

                AddressRepository objAddressRepository = new AddressRepository();
                objAddressInfo = objAddressRepository.Get(objAddressInfoRequest);
            }
            catch
            {
                objAddressInfo = null;
            }
            return objAddressInfo;
        }

        public static AddressInfo GetAddressInfoByAddressID(string strAddressID)
        {
            AddressInfo objAddressInfo = new AddressInfo();
            try
            {
                AddressRequest objInput = new AddressRequest();
                objInput.AddressId = strAddressID;
                objInput.ClientID = short.Parse(ClientID);
                objInput.SystemID = short.Parse(SystemID);

                AddressRepository objAddressRepository = new AddressRepository();
                objAddressInfo = objAddressRepository.GetInfoAddressId(objInput);
            }
            catch
            {
                objAddressInfo = null;
            }
            return objAddressInfo;
        }

        public static TransactionInfo Tranfer(string fromID, string fromAddress, string toID, decimal value, string strDescription = "")
        {
            TransactionInfo objTransactionInfo = new TransactionInfo();
            try
            {
                TransferRequest objTransferRequest = new TransferRequest();
                objTransferRequest.UserId = fromID;
                objTransferRequest.FromAddress = fromAddress;
                objTransferRequest.ClientID = short.Parse(ClientID);
                objTransferRequest.SystemID = short.Parse(SystemID);
                objTransferRequest.Amount = value;
                objTransferRequest.Description = strDescription;
                objTransferRequest.ToClientId = short.Parse(ClientID);
                objTransferRequest.ToSystemId = short.Parse(SystemID);
                objTransferRequest.ToUserId = toID;
                objTransferRequest.CurrencyCode = CurrencyCode.BTC;
                objTransferRequest.CreatedBy = RequestBy;
                objTransferRequest.IsTransferLimit = true;


                TransactionRepository objTransactionRepository = new TransactionRepository();

                objTransactionInfo = objTransactionRepository.Transfer(objTransferRequest);
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText("Error Common(Tranfer) Message: " + ex.Message);
                objTransactionInfo = null;
            }
            return objTransactionInfo;
        }


        public static string TransferPromotion(UserPromoteDepositRequest objItem)
        {
            string transactionID = string.Empty;
            try
            {
                //TransferRequest objTransferRequest = new TransferRequest();
                //objTransferRequest.UserId = fromID;
                //objTransferRequest.FromAddress = fromAddress;
                //objTransferRequest.ClientID = short.Parse(ClientID);
                //objTransferRequest.SystemID = short.Parse(SystemID);
                //objTransferRequest.Amount = value;
                //objTransferRequest.Description = strDescription;
                //objTransferRequest.ToClientId = short.Parse(ClientID);
                //objTransferRequest.ToSystemId = short.Parse(SystemID);
                //objTransferRequest.ToUserId = toID;
                //objTransferRequest.CurrencyCode = CurrencyCode.BTC;
                //objTransferRequest.CreatedBy = RequestBy;
                //objTransferRequest.IsTransferLimit = true;


                TransactionRepository objTransactionRepository = new TransactionRepository();

                //objTransactionInfo = objTransactionRepository.Transfer(objTransferRequest);
                transactionID = objTransactionRepository.DepositPromote(objItem);
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText("Error Common(Tranfer) Message: " + ex.Message);
                //objTransactionInfo = null;
            }
            return transactionID;
        }

    }
}