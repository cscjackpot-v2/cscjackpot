﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BBH.Lotte.Web.Models
{
    public class TransferPromotionModel
    {
        [Required(ErrorMessage = "The field is required.")]
        [Remote("ValidateToUser", "Validate", AdditionalFields = "ToUser,IsEmailTransfer")]
        public string ToUser { get; set; }

        public bool IsEmailTransfer { get; set; }

        [Required(ErrorMessage = "Cannot be empty.")]
        [RegularExpression(@"^\d*\.?\d*$", ErrorMessage = "Amount need to be present and less or equal to available balance!")]
        [Remote("CheckAvailablePromotion", "Validate")]
        [Range(0.00000001, 1, ErrorMessage = "Tranfer amount: 0.00000001 to 1 BTC.")]
        public decimal AmountPromotion { get; set; }

        public string Description { get; set; }
    }
}