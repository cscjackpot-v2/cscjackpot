﻿using BBC.Core.WebService;
using BBC.CWallet.ServiceListener.ServiceContract;
using BBC.CWallet.ServiceListener.ServiceContract.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.Lotte.CLP.Wallet.Repository
{
    public class AccountRepository : WCFClient<IAccountServices>, IAccountServices
    {
        public AccountInfo Create(AccountCreationRequest objInput)
        {
            return Proxy.Create(objInput);
        }

        public AccountInfo Get(AccountInfoRequest objInput)
        {
            return Proxy.Get(objInput);
        }

        public CWalletList<AccountInfo> List(SystemFilterRequest objInput)
        {
            return Proxy.List(objInput);
        }

        public CWalletList<AccountEventInfo> ListEventsHistory(AccountEventFilterRequest objInput)
        {
            return Proxy.ListEventsHistory(objInput);
        }

        public AccountEventInfo Lock(AccountLockRequest objInput)
        {
            return Proxy.Lock(objInput);
        }

        public AccountEventInfo UnLock(AccountLockRequest objInput)
        {
            return Proxy.UnLock(objInput);
        }

        public AccountInfo Update(AccountEditionRequest objInput)
        {
            return Proxy.Update(objInput);
        }
    }
}
