﻿using BBC.Core.Common.IoC;
using BBH.Lotte.CMSAdmin;
using System;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace BBH.Lotte.CLP.CMSAdmin
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            //WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            // AuthConfig.RegisterAuth();
            DependencyFactory.Initialize();
        }

        protected void Application_Error()
        {
            Exception exc = Server.GetLastError();
            BBC.CWallet.Share.BBCLoggerManager.Error("error", exc);
        }
    }
}
