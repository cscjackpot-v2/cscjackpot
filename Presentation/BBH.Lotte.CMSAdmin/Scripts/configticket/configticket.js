﻿function LockAndUnlockConfig(configID, status) {
    var confirmMessage = confirm('Are you sure lock this config?');
    if (!confirmMessage) {
        return false;
    }
    else {

        $.ajax({
            type: "post",
            async: true,
            url: "/TicketConfig/LockAndUnLockTicketConfig",
            data: { configID: configID, status: status },
            beforeSend: function () {
                showLoading();
            },
            success: function (d) {
                hideLoading();
                if (d == 'success') {
                    window.location.reload();
                } else {
                    noty({ text: "Update Error", layout: "bottomRight", type: "error" });
                }
            },
            error: function (e) {

            }
        });
    }
}
 
function ShowPopUpEditTicketConfig(configID,coinfigName, numberTicket, coinValue, coinID) {
    $('#hdConfigID').val(configID);
    //$('#txtAwardName').val(awardName);
    $('#txtConfigName').val(coinfigName);
    $('#txtNumberTicket').val(numberTicket);
    $('#txtCoinvalues').val(coinValue);
    $('#txtCoinID').val(coinID);

    //$('#txtAwardName').attr("readonly", true);
    //$('#txtAwardNameEnglish').attr("readonly", true);
}

function ShowPopupCreateNewConfigTicket() {
    $('#hdConfigID').val(0);
    //$('#txtAwardName').val(awardName);
    $('#txtConfigName').val('');
    $('#txtNumberTicket').val('');
    $('#txtCoinvalues').val('');
    $('#txtCoinID').val('');
    //$('#txtAwardName').attr("readonly", false);
    //$('#txtAwardNameEnglish').attr("readonly", false);
}

function ResetForm(id, value) {
    $('#' + id).text('');
    $('#' + id).css('display', 'none');
}
function onlyNumber(evt) {
    var e = event || evt;
    var charCode = e.which || e.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode == 45)
        return false;
    return true;
}
function SaveConfigTicket() {
    var checkReg = true;
    var configID = $('#hdConfigID').val();
    var coinfigName = $('#txtConfigName').val();
    var numberTicket = $('#txtNumberTicket').val();
    var coinValue = $('#txtCoinvalues').val();
    var coinID = $('#txtCoinID').val();


    if (coinfigName == '') {
        $('#lbErrorConfigName').text('ConfigName is required');
        $('#lbErrorConfigName').css('display', '');
        checkReg = false;
    }
    if (numberTicket == '') {
        $('#lbErrorNumberTicket').text('Number ticket is required');
        $('#lbErrorNumberTicket').css("display", "");
        checkReg = false;
    }
    if (coinValue == '') {
        $('#lbErrorCoinValue').text('Coin value is required');
        $('#lbErrorCoinValue').css("display", "");
        checkReg = false;
    }
    if (coinID == '') {
        $('#lbErrorCoinID').text('CoinID is required');
        $('#lbErrorCoinID').css("display", "");
        checkReg = false;
    }
    if (!checkReg) {
        return false;
    }
    else {
        $.ajax({
            type: "post",
            async: true,
            url: "/TicketConfig/SaveTicketConfig",
            data: { configID: configID, coinfigName: coinfigName, numberTicket: numberTicket, coinValue: coinValue, coinID: coinID },
            beforeSend: function () {
                showLoading();
            },
            success: function (d) {
                hideLoading();
                if (d == 'success') {
                    noty({ text: "Update Success", layout: "bottomRight", type: "information" });
                    setTimeout(function () { window.location.reload(); }, 2000);
                }
                else if (d == 'failes') {
                    noty({ text: "Update Error", layout: "bottomRight", type: "error" });
                }
                else if (d == 'CONFIGTICKETEXITS') {
                    $('#lbErrorCoinID').text('CoinID Exit');
                    $('#lbErrorCoinID').css("display", "");
                    window.scrollTo(100, 100);
                }


            },
            error: function (e) {

            }
        });
    }
}