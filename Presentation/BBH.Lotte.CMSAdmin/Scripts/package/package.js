﻿function ConfirmTransaction(transactionID, status, memberID, packageID) {

    var textMessage = '';
    if (status == 1) {
        textMessage = 'Are you sure confirm this transaction?';
    }
    else if (status == 2) {
        textMessage = 'Are you sure lock this transaction?';
    }
    var confirmMessage = confirm(textMessage);
    if (!confirmMessage) {
        return false;
    }
    else {
        $('#imgLoading_' + transactionID).css("display", "");
        $.ajax({
            type: "post",
            async: false,
            url: "/Package/UpdateStatusTransaction",
            data: { transactionID: transactionID, status: status, memberID: memberID, packageID: packageID },
            beforeSend: function () {
                $('#imgLoading_' + transactionID).css("display", "");
            },
            success: function (d) {
                $('#imgLoading_' + transactionID).css("display", "none");
                if (d == 'success') {

                    noty({ text: "Update Success", layout: "bottomRight", type: "information" });
                    setTimeout(function () { window.location.reload(); }, 2000);
                }
                else {
                    noty({ text: "Update Error. Please contact admin", layout: "bottomRight", type: "error" });
                }
            },
            error: function (e) {

            }
        });
    }
}

function SearchPackage() {
    var memberID = $('#cbMember').val();
    var fromDate = $('#txtFromDate').val();
    var toDate = $('#txtToDate').val();
    var status = $('#cbStatus').val();
    var page = 1;
    $.ajax({
        type: "get",
        async: true,
        url: "/Package/SearchPackage",
        data: { memberID: memberID, fromDate: fromDate, toDate: toDate, page: page, status: status },
        beforeSend: function () {
            $('#imgLoadingSearch').css("display", "")
        },
        success: function (d) {
            $('#imgLoadingSearch').css("display", "none")

            var json = JSON.parse(d);
            if (json != "") {
                $('#tbodyListPackage').html(json.ContentResult);
                $('#ulPaging').html(json.PagingResult);
            }
        },
        error: function (e) {

        }
    });
}

function PagingSearchTransactionPackage(page) {
    var memberID = $('#cbMember').val();
    var fromDate = $('#txtFromDate').val();
    var toDate = $('#txtToDate').val();
    var status = $('#cbStatus').val();
    $.ajax({
        type: "get",
        async: true,
        url: "/Package/SearchPackage",
        data: { memberID: memberID, fromDate: fromDate, toDate: toDate, page: page, status: status },
        beforeSend: function () {
            $('#imgLoadingSearch').css("display", "")
        },
        success: function (d) {
            $('#imgLoadingSearch').css("display", "none")
            var json = JSON.parse(d);
            if (json != "") {
                $('#tbodyListPackage').html(json.ContentResult);
                $('#ulPaging').html(json.PagingResult);
            }
        },
        error: function (e) {

        }
    });
}

function ExportExcelPackage()
{
    var memberID = $('#cbMember').val();
    var fromDate = $('#txtFromDate').val();
    var toDate = $('#txtToDate').val();
    var status = $('#cbStatus').val();
    var page = 1;
    $.ajax({
        type: "get",
        async: true,
        url: "/Package/ExportListTransactionPackage",
        data: { memberID: memberID, fromDate: fromDate, toDate: toDate, page: page, status: status },
        beforeSend: function () {
            $('#imgLoadingSearch').css("display", "")
        },
        success: function (d) {
            $('#imgLoadingSearch').css("display", "none")

            window.location.href = '/FileExcels/TransactionPackage/' + d;
        },
        error: function (e) {

        }
    });
}