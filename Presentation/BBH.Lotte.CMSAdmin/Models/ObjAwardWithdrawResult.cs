﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BBH.Lotte.CLP.CMSAdmin.Models
{
    public class ObjAwardWithdrawResult
    {
        public string TransactionID { get; set; }

        public string RequestDate { get; set; }

        public string AwardDate { get; set; }

        public string RequestStatus { get; set; }

        public string UserName { get; set; }

        public string AwardValues { get; set; }
        public string ConfirmDate { get; set; }

        public string ConfirmUser { get; set; }
    }
}