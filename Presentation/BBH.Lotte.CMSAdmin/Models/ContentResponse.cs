﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BBH.Lotte.CLP.CMSAdmin.Models
{
    public class ContentResponse
    {
        public int id { get; set; }
        public string email { get; set; }
        public string value { get; set; }
        public string date_time { get; set; }
    }

    public class ApiUpdateTicketResponse
    {
        public string result { get; set; }
        public List<object> sucess_list { get; set; }
    }
}