﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BBH.Lotte.CMSAdmin.Models
{
    public class ResultKey
    {
        public const string RESULT_USERNAMENULL = "UserNameNull";
        public const string RESULT_PASSWORDNULL = "PasswordNull";
        public const string RESULT_EMAILNULL = "EmailNull";
        public const string RESULT_MOBILENULL = "MobileNull";
        public const string RESULT_ADDRESSNULL = "AddressNull";
        public const string RESULT_FULLNAMENULL = "FullNameNull";
        public const string RESULT_SUCCESS = "success";
        public const string RESULT_FAILES = "failes";
        public const string RESULT_EXISTUSERNAME = "UserNameExist";
        public const string RESULT_EXISTEMAIL = "EmailExist";
        public const string RESULT_EXISTCREATEDATE = "CreateDateExist";
        public const string RESULT_GROUPNAMENULL = "GroupNameNull";
        public const string RESULT_GROUPNAMEEXIST = "GroupNameExist";

        public const string RESULT_PHONECODEEXIST = "PhoneCodeExist";
 
        public const string RESULT_AWARDNAMEENGLISHNULL = "AwardNameEnglishNull";
        public const string RESULT_AWARDVALUENULL = "AwardValueNull";
        public const string RESULT_AWARDNAMEENGLISHEXIST = "AwardNameEnglishExit";
        public const string RESULT_STRBLOCKHASHNULL = "strBlockHashNull";

        public const string RESULT_CHECKTIMEOPENCLOSE = "CheckTimeOpenClose";

        public const string RESULT_BITCOINVALUESNULL = "bitcoinValueNull";
        public const string RESULT_POINTVALUESNULL = "pointValueNull";

        public const string RESULT_LOGINSUCCESS = "loginSuccess";
        public const string RESULT_LOGINFAILES = "loginfaile";

        //ExChangeTicketCoin
        public const string RESULT_POINTVALUENULL = "PointValueNull";
        public const string RESULT_TICKETNUMBERNULL = "TicketNumberNull";
        public const string RESULT_COINIDNULL = "CoinIDNull";
        public const string RESULT_COINIDEXIST = "CoinIDExist";
        public const string RESULT_COUNTRYNAMEEXIST = "CountryNameExist";

        public const string RESULT_CONFIGTICKETEXIST = "CONFIGTICKETEXITS";
        public const string RESULT_EXISTS= "Result exists";
    }
}