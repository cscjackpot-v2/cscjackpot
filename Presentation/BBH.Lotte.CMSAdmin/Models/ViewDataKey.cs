﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BBH.Lotte.CMSAdmin.Models
{
    public class ViewDataKey
    {
        public const string VIEWDATA_LISTADMIN = "ListAdmin";
        public const string VIEWDATA_LISTGROUP = "ListGroupAdmin";
        public const string VIEWDATA_LISTAWARD = "ListAward";
        public const string TEAMDATA_TOTALRECORD = "TotalRecord";
        public const string TEAMDATA_TOTALRECORDWINNER = "TotalRecord";

        public const string NUMBER49 = "Number49";
        public const string NUMBER26 = "Number26";
        public const string VIEWDATA_LISTAWARDMEGABALL = "ListAwardMegaball";
        public const string VIEWDATA_LISTMEMBER = "ListMember";
        public const string VIEWDATA_LISTAWARDNUMBER = "ListAwardNumber";
        public const string VIEWDATA_LISTWINERAWARD = "ListWinnerAward";
        public const string VIEWDATA_LISTBOOKINGMEGABALL = "ListBookingMegaball";
        public const string VIEWDATA_TRANSACTIONPOINT = "TransactionPoints";
        public const string VIEWDATA_TRANSACTIONCOIN = "TransactionCoin";
        public const string VIEWDATA_LISTEXCHANGETICKET = "ListExchangeTicket";
        public const string VIEWDATA_LISTEXCHANGEPOINT = "ListExchangePoint";
        public const string VIEWDATA_LISTCOMPANY = "ListCompany";
        public const string VIEWDATA_LISTCOIN = "ListCoin";
        public const string VIEWDATA_JACKPOTDEFAULT = "jackPotDefault";
        public const string VIEWDATA_LISTTICKETCONFIG = "ListTicketConfig";
        public const string VIEWDATA_LISTSYSTEMCONFIG = "ListSystemConfig";
        public const string VIEWDATA_LISTCOUNTRY = "ListCountry";
    }
}