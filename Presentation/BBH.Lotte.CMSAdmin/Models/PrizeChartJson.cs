﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BBH.Lotte.CLP.CMSAdmin.Models
{
    public class PrizeChartJson
    {
        public string label { get; set; }
        public double data { get; set; }
    }
}