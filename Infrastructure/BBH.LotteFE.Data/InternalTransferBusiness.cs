﻿using BBC.Core.Common.Log;
using BBC.Core.Database;
using BBH.LotteFE.Domain;
using BBH.LotteFE.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.LotteFE.Data
{
    public class InternalTransferBusiness: IInternalTransferServices
    {
        public ObjResultMessage InsertInternalTransfer(InternalTransferBO objInternalTransfer)
        {
            ObjResultMessage objResult = new ObjResultMessage();
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<InternalTransferBO> lstInternalTransferBO = new List<InternalTransferBO>();
                string sql = "SP_InsertInternalTransfer";
                SqlParameter[] pa = new SqlParameter[9];
                pa[0] = new SqlParameter("@TransactionID", objInternalTransfer.TransactionID);
                pa[1] = new SqlParameter("@MemberID", objInternalTransfer.MemberID);
                pa[2] = new SqlParameter("@FromAddress", objInternalTransfer.FromAddress);
                pa[3] = new SqlParameter("@ToAddress", objInternalTransfer.ToAddress);
                pa[4] = new SqlParameter("@Amount", objInternalTransfer.Amount);
                pa[5] = new SqlParameter("@TransactionType", objInternalTransfer.TransactionType);
                pa[6] = new SqlParameter("@Description", objInternalTransfer.Description);
                pa[7] = new SqlParameter("@TransferDate", objInternalTransfer.TransferDate);
                pa[8] = new SqlParameter("@TransactionStatus", objInternalTransfer.TransactionStatus);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    objResult.IsError = false;
                }
            }
            catch (Exception ex)
            {
                objResult.IsError = true;
                objResult.Result = MessageResult.Error;
                objResult.MessageDetail = "InternalTransferBusiness -> InsertInternalTransfer : " + ex.Message;
                objResult.MessageDate = DateTime.Now;
                WriteErrorLog.WriteLogsToFileText(ex.Message);
            }
            finally
            {
                helper.destroy();
            }
            return objResult;
        }
    }
}
