﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace BBH.LotteFE.Domain.Interfaces
{
    [ServiceContract]
    public interface IBookingServices
    {
        [OperationContract]
        bool InsertBooking(BookingBO booking);
        
        [OperationContract]
        IEnumerable<BookingBO> ListTransactionBookingByMember(int memberID, int start, int end);
        
        [OperationContract]
        IEnumerable<BookingBO> ListTransactionBookingBySearch(int memberID, DateTime? fromDate, DateTime? toDate, int start, int end);
        [OperationContract]
        IEnumerable<BookingBO> ListTransactionBookingByDate(DateTime fromDate, DateTime toDate);
        [OperationContract]
        bool UpdateStatusBooking(int bookingID, int status);
        [OperationContract]
        int CountMemberBuyNumber(int memberID, string numberValue, DateTime fromDate, DateTime toDate);

    }
}
