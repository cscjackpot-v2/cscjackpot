﻿using BBC.Core.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace BBH.LotteFE.Domain.Interfaces
{
    [ServiceContract]
    public interface IInternalTransferServices
    {
        [OperationContract]
        ObjResultMessage InsertInternalTransfer(InternalTransferBO objInternalTransfer);
    }
}
