﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BBH.LotteFE.Domain
{
    [DataContract]
    [Serializable]
    public class InternalTransferBO
    {
        [DataMember]
        public string TransactionID { get; set; }
        [DataMember]
        public int MemberID { get; set; }
        [DataMember]
        public string FromAddress { get; set; }
        [DataMember]
        public string ToAddress { get; set; }
        [DataMember]
        public decimal Amount { get; set; }
        [DataMember]
        public string TransactionType { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public DateTime TransferDate { get; set; }
        [DataMember]
        public int TransactionStatus { get; set; }
    }
}
