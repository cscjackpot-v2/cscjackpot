﻿using BBC.Core.Database;
using BBC.Core.WebService;
using BBH.Lotte.Domain;
using BBH.Lotte.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.Lotte.Repository
{
    public class DrawRepository : WCFClient<IDrawServices>, IDrawServices
    {
        public DrawBO GetDrawIDByDate(DateTime dtTime)
        {
            try
            {
                return Proxy.GetDrawIDByDate(dtTime);
            }
            catch
            {
                return null;
            }
        }

        public DrawBO GetNewestDrawID()
        {
            try
            {
                var abc = Proxy.GetNewestDrawID();
                BBC.CWallet.Share.BBCLoggerManager.Debug("GetNewestDrawID: " + abc);
                return abc;
            }
            catch (Exception objEx)
            {
                BBC.CWallet.Share.BBCLoggerManager.Debug("GetNewestDrawID -> Excep: " + objEx);
                return null;
            }
        }

        public ObjResultMessage InsertDraw(DrawBO objDraw)
        {
            try
            {
                return Proxy.InsertDraw(objDraw);
            }
            catch
            {
                return null;
            }
        }
    }
}
