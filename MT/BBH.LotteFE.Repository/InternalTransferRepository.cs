﻿using BBC.Core.Database;
using BBC.Core.WebService;
using BBH.LotteFE.Domain;
using BBH.LotteFE.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.LotteFE.Repository
{
    public class InternalTransferRepository : WCFClient<IInternalTransferServices>, IInternalTransferServices
    {
        public ObjResultMessage InsertInternalTransfer(InternalTransferBO objInternalTransfer)
        {
            try
            {
                return Proxy.InsertInternalTransfer(objInternalTransfer);
            }
            catch
            {
                return null;
            }
        }
    }
}
