﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using BBH.Lotte.CLP.Domain;

namespace BBH.Lotte.CLP.WCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "NumberGenerralSvc" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select NumberGenerralSvc.svc or NumberGenerralSvc.svc.cs at the Solution Explorer and start debugging.
    public class NumberGenerralSvc : INumberGenerralSvc
    {

        public bool InsertNumberGeneral(NumberGeneralBO number)
        {
            return BBH.Lotte.CLP.Data.NumberGeneralBusiness.InsertNumberGeneral(number);
        }

        public bool UpdateNumberGeneral(string numberValue, int quantity, int numberID)
        {
            return BBH.Lotte.CLP.Data.NumberGeneralBusiness.UpdateNumberGeneral(numberValue, quantity, numberID);
        }

        public bool UpdateQuantityNumberGeneral(string numberValue, int quantity)
        {
            return BBH.Lotte.CLP.Data.NumberGeneralBusiness.UpdateQuantityNumberGeneral(numberValue, quantity);
        }

        public bool LockAndUnlockNumberGeneral(int numberID, int isActive)
        {
            return BBH.Lotte.CLP.Data.NumberGeneralBusiness.LockAndUnlockNumberGeneral(numberID, isActive);
        }

        public NumberGeneralBO GetNumberGeneralDetail(int numberID)
        {
            return BBH.Lotte.CLP.Data.NumberGeneralBusiness.GetNumberGeneralDetail(numberID);
        }

        public bool CheckNumberExists(string numberValue)
        {
            return BBH.Lotte.CLP.Data.NumberGeneralBusiness.CheckNumberExists(numberValue);
        }

        public IEnumerable<NumberGeneralBO> GetListNumberGeneralPaging(int start, int end)
        {
            return BBH.Lotte.CLP.Data.NumberGeneralBusiness.GetListNumberGeneral(start, end);
        }

        public IEnumerable<NumberGeneralBO> GetListNumberGeneral(int start, int end, DateTime fromDate, DateTime toDate)
        {
            return BBH.Lotte.CLP.Data.NumberGeneralBusiness.GetListNumberGeneral(start, end, fromDate, toDate);
        }
    }
}
