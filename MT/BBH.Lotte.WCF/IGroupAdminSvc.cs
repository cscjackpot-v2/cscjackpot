﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using BBH.Lotte.CLP.Domain;

namespace BBH.Lotte.CLP.WCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IGroupAdminSvc" in both code and config file together.
    [ServiceContract]
    public interface IGroupAdminSvc
    {
        [OperationContract]
        bool InsertGroupAdmin(GroupAdminBO admin);

        [OperationContract]
        IEnumerable<GroupAdminBO> ListGroupAdmin();

        [OperationContract]
        GroupAdminBO GetGroupAdminDetail(int groupID);

        [OperationContract]
        bool UpdateStatusGroupAdmin(int groupID, int isActive);

        [OperationContract]
        bool UpdateGroupAdmin(int groupID, string groupName);

    }
}
