﻿using BBH.Lotte.CLP.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace BBH.Lotte.CLP.WCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IAwardMegaballSvc" in both code and config file together.
    [ServiceContract]
    public interface IAwardMegaballSvc
    {
        [OperationContract]
        bool InsertAwardMegaball(AwardMegaballBO awardMegaball);

        [OperationContract]
        bool UpdateAwardMegaball(AwardMegaballBO awardMegaball);

        [OperationContract]
        bool LockAndUnlockAwardMegaball(int numberID, int isActive);

        [OperationContract]
        IEnumerable<AwardMegaballBO> GetListAwardMegaball(int start, int end);

        [OperationContract]
        IEnumerable<AwardMegaballBO> ListAllAwardMegaballBySearch(DateTime fromDate,DateTime toDate, int start, int end);

    }
}
